####################
MapleTax Entry Point
####################


This project is about bringing Free Software tools for submitting a
Canadian income declaration.

Given  that `e-filing`_ is not reasonably achievable as of now, we're
for now preparing data that would be sent by the post ; the forms are
filled automatically and prepared for print-outs and snail-mail
transmission to the government agencies.


Usage
#####

Preliminary instructions:

- clone all mapletax repos under a mapletax folder, typically
  `~/.local/opt/mapletax`,
  and add the enclosing directory (eg. `~/.local/opt`) to
  `PYTHONPATH`.

- see some examples.



Canada Revenue Agency
*********************


Revenu Québec - TP-1.D
**********************



Design
######


The following components are used:

- `Form Preprocessing`_: for each form and annex that needs to be filled
  or transmitted, the localization and information about fields (a
  field is given an identifier, and we need to know how to complete
  it) needs to be provided ; (this could be done by hand or automatically),
  the form preprocessing tooling outputs form filling metadata files
  that lay in this repository.

- `Computations`_: The CRA and RQ formulas are implemented,
  using symbolic representations.

- `Edition`_: For now the scope is to have the user use a text editor
  (eg. YAML, TOML) to manipulate their information.

  GUI helpers are to be discussed.

- `Form Filler`_. At the end, the PDF forms are filled with the final
  data.


Form Preprocessing
******************

The `poutine <https://gitlab.com/mapletax/poutine>`_ tooling
provides the means to parse forms, via a more generic
`formparse <https://gitlab.com/mapletax/formparse>`_ library.

Notably:

- For Revenu Québec forms, for their numeric fields, we rely on
  presence of a comma, on the right of a numeric field identifier.


Form Filler
***********

The `formfill <https://gitlab.com/mapletax/formfill>`_ tooling
provides the means to fill forms, given metadata (where are the fields,
how the content would be printed), and data (what is the field content).


E-filing
********

The Canada Revenue Agency expects e-filing of income declarations to be
done only using certified software [citation needed].

Revenu Québec expects e-filing of income returns to be
done only using certified software [citation needed].


Computations
************

Computations are implemented symbolically.

Each form has its "form namespace" implementing a system of
equations/relations.

The declaration computations are in the
`toonies <https://gitlab.com/mapletax/toonies>`_
module, which makes use of primitives in
`loonies <https://gitlab.com/mapletax/loonies>`_
.


Edition
*******

By hand for now. That shouldn't be a problem for those used to
plain-text accounting.

For example you could work in a Jupyter notebook, importing the mapletax
code, setting up your slips and info, and generating the declaration.


License
#######

This tooling is aimed to be copyleft and such that any user can access
its source. Thus we have chosen to use AGPLv3 ; also we don't want the
tool to be able to become proprietary so the code uses a developer
certificate of origin, where each contributor is reponsible of, and
has copyright on their contribution.

